consult('/home/jmesuro/ztosetlog/setlog/setlog495-16b.pl').
set_prolog_flag(toplevel_print_options, [quoted(true), portray(true)]).
use_module(library(dialect/sicstus/timeout)).
set_prolog_flag(answer_write_options, [quoted(true), portray(true),max_depth(0)]).
time_out(setlog( 
NAT = int(0, 2147483649) & 
NAT1 = int(1, 2147483647) & 
INT = int(-2147483648, 2147483647) & 
SRVTYPE = {cc105HS,cc109SC,cc118COM,cc128VOM,cc110SDA,cc108RDA,cc126TD,cc125RA,cc122GMR,cc113LDM,cc120EP,cc121MSP} & 
BIN = {cc100NO,cc102YES} & 
PDCANS = {cc124CR,cc119CD,cc116OM,cc129ND,cc112ASD,cc115AMD,cc117MSE,cc130ICS,cc103LS} & 
DTYPE = {cc111SD,cc131HD,cc114MD} & 
Srv in SRVTYPE & 
PrepData in BIN & 
LpckDT in PDCANS & 
set(ModMem) & 
ProcessingCmd in BIN & 
IaInput in NAT & 
pfun(DataInput) & 
dom(DataInput,AUX0) & 
AUX0 = int(1,AUX1) & 
AUX1 in NAT & 
PrepDataType in DTYPE & 
ris(I in AUX0,[AUX2],AUX2 in INT & AUX2 is IaInput + I,AUX2) neq {} & 
ModMem = {}
,_CONSTR),1000,_RET).

