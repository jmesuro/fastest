package client.blogic.testing.ttree;

import java.io.*;
import java.util.concurrent.locks.*;
import java.util.*;

import client.blogic.management.ii.events.*;
import client.blogic.testing.ttree.visitors.SpecCleaner;
import common.z.*;
import common.z.czt.UniqueZLive;
import common.z.czt.visitors.TClassNodeUnfolder;
import net.sourceforge.czt.session.*;
import net.sourceforge.czt.typecheck.z.ErrorAnn;
import net.sourceforge.czt.typecheck.z.TypeCheckUtils;
import net.sourceforge.czt.z.ast.ParaList;
import net.sourceforge.czt.z.ast.ZParaList;
import net.sourceforge.czt.z.ast.Sect;
import net.sourceforge.czt.z.ast.ZSect;
import net.sourceforge.czt.z.ast.Spec;
import net.sourceforge.czt.z.ast.AxPara;

import client.blogic.management.ii.EventAdmin;
import client.blogic.management.ii.IIComponent;
import client.blogic.testing.ttree.tactics.Tactic;
import client.blogic.testing.ttree.strategies.TTreeStrategy;

import client.presentation.ClientUI;
import client.blogic.management.Controller;

/**
 * Instances of this class (although we assume there is only one in the 
 * system) manage events of type TTreeRequested and order the generation of test
 * trees.  
 * @author Pablo Rodriguez Monetti
 */
public class TTreeGen extends IIComponent {

    private Spec spec;
    private String unitToTest;
    private TClassNode tClassNode;
    private List<Tactic> tacticList;
    private TTreeStrategy ttreeStrategy;
    private ZParaList zParaList;
    private Lock myLock;

    /**
     * Creates new instances of TTreeGen.
     */
    public TTreeGen() {
        myLock = new ReentrantLock();
    }

    /**
     * Manages an implicit invocation event.
     *
     * @param event_
     * @throws java.lang.IllegalArgumentException if event_ is neither instance of
     *                                            SpecLoaded nor TTreeRequested.
     */
    public void manageEvent(Event_ event_)
            throws IllegalArgumentException {

        myLock.lock();

        if (event_ instanceof TTreeRequested) {

            TTreeRequested tTreeRequested = (TTreeRequested) event_;
            unitToTest = tTreeRequested.getUnitToTest();
            tacticList = tTreeRequested.getTacticList();
            ttreeStrategy = tTreeRequested.getTTreeStrategy();
            tClassNode = tTreeRequested.getTClassNode();

            if (spec != null) {
                generateTTree2();
            } else {
                System.out.println("The specification was not correctly loaded.");
            }

        } else if (event_ instanceof SpecLoaded) {
            spec = ((SpecLoaded) event_).getSpec();
        } else {
            throw new IllegalArgumentException();
        }
        myLock.unlock();
    }

    private boolean tTreeTypeCheck(TTreeNode rootNode) {
        Controller controller = myClientUI.getMyController();
        TClassNode tClassNodeIt;
        TClassNodeUnfolder tClassNodeUnfolder;
        Spec spec;
        for (TTreeNode node : rootNode.getChildren()) {

            tClassNodeIt = (TClassNode) node;


            tClassNodeUnfolder = new TClassNodeUnfolder(tClassNodeIt, controller);
            tClassNodeIt.acceptVisitor(tClassNodeUnfolder);
            TClass tClass = tClassNodeUnfolder.getTClassUnfolded();

            spec = (Spec) this.spec.accept(new SpecCleaner(tClass.getMyAxPara(), this.spec));





            String texFileName = "specOpTemp.tex";
            String texFileToReadName = "specOpTemp2.tex";

            copyFile(spec,texFileName,texFileToReadName);


            controller.setNomTexFileSpec(texFileName);

            File texFileToRead = new File(texFileToReadName);
            FileSource source = new FileSource(texFileToRead);

            SectionManager manager = new SectionManager();
            manager.put(new Key(texFileToReadName, Source.class), source);

            Spec spec2 = null;
            try {
                spec2 = (Spec) manager.get(new Key(texFileToReadName, Spec.class));
            } catch (CommandException e) {
                e.printStackTrace();
            }


            List<? extends ErrorAnn> errors =
                    TypeCheckUtils.typecheck(spec2, manager, true);
            if (errors.size() > 0) {
                System.out.println("TTree has not been "
                        + "generated because it has type errors. This may be caused by an error using addtactic.");
                for (int i = 0; i < errors.size(); i++) {
                    String errorStr = errors.get(i).toString();
                    errorStr = errorStr.substring(errorStr.indexOf(","));
                    errorStr = tClass.getSchName() + errorStr;
                    System.out.println(errorStr + "\n");
                }
                return false;
            }


//
//            List<? extends ErrorAnn> errors =
//                    TypeCheckUtils.typecheck(spec, UniqueZLive.getInstance().getSectionManager(),true );
//
//            if (errors.size() > 0) {
//                System.out.println("TTree has not been "
//                        + "generated because it has type errors. This may be caused by an error using addtactic.");
//                for (int i = 0; i < errors.size(); i++) {
//                    String errorStr = errors.get(i).toString();
//                    errorStr = errorStr.substring(errorStr.indexOf(","));
//                    errorStr = tClass.getSchName() + errorStr;
//                    System.out.println(errorStr + "\n");
//                }
//                return false;
//            }

          /*  String texFileToReadName = writeToFile(SpecUtils.termToLatex(spec));
            File texFileToRead = new File(texFileToReadName);

            FileSource source = new FileSource(texFileToRead);

            SectionManager manager = new SectionManager();
            manager.put(new Key(texFileToReadName, Source.class), source);

            try {
                Spec spec2 = (Spec) manager.get(new Key(texFileToReadName, Spec.class));
                List<? extends ErrorAnn> errors =
                        TypeCheckUtils.typecheck(spec, UniqueZLive.getInstance().getSectionManager(),false );
                if (errors.size() > 0) {
                    System.out.println("TTree has not been "
                            + "generated because it has type errors. This may be caused by an error using addtactic.");
                    for (int i = 0; i < errors.size(); i++) {
                        String errorStr = errors.get(i).toString();
                        errorStr = errorStr.substring(errorStr.indexOf(","));
                        errorStr = tClass.getSchName() + errorStr;
                        System.out.println(errorStr + "\n");
                    }
                    return false;
                }
            } catch (CommandException e) {
                e.printStackTrace();
            }*/


            File specOpTempFile = new File("specOpTemp.tex");
            File specOpTemp2File = new File("specOpTemp2.tex");

            specOpTempFile.delete();
            specOpTemp2File.delete();


        }
        return true;
    }

    private void generateTTree2() {

        if (tClassNode == null) {
            // The test tree must be generated from the beginning
            System.out.println("Generating test tree for '" + unitToTest + "' operation.");

            for (Sect sect : spec.getSect()) {
                if (sect instanceof ZSect) {
                    ParaList paraList = ((ZSect) sect).getParaList();
                    if (paraList instanceof ZParaList) {
                        zParaList = (ZParaList) paraList;
                    }
                }
            }

            AxPara opAxPara = SpecUtils.axParaSearch(unitToTest, (ZParaList) zParaList);
            OpScheme opScheme = new OpSchemeImpl(opAxPara);
            // References to the SectionManager and to the SectionName are
            // obtained
            ClientUI clientUI = getMyClientUI();
            Controller controller = clientUI.getMyController();

            VISGen.setController(controller);
            VISGen.setZParaList(zParaList);

            tClassNode = ttreeStrategy.generateRootNode(opScheme, zParaList);
        }

        TClassNode newNode = ttreeStrategy.generateTTree(tClassNode, tacticList);

        if (!tTreeTypeCheck(tClassNode)) {
            tClassNode.setChildren(new ArrayList<TTreeNode>());
            newNode = null;
        }


        try {
            EventAdmin eventAdmin = EventAdmin.getInstance();
            TTreeGenerated tTreeGenerated = new TTreeGenerated(unitToTest, newNode);
            eventAdmin.announceEvent(tTreeGenerated);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private String writeToFile(String spec) {
        String name = "specOpTemp.tex";
        try {

            FileWriter myWriter = new FileWriter(name);

            myWriter.write(spec);
            myWriter.close();
            return name;
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return name;
    }

    /**
     * This method copies the content of the file, whose name is equals to the
     * first specified string, into the file whose name is equals to the second
     * specified string.
     */
    private boolean copyFile(Spec spec,String originalFileName,String newFileName) {
        String specStr= SpecUtils.termToLatex(spec);


        try {

            FileWriter myWriter = new FileWriter(originalFileName);

            myWriter.write(specStr);
            myWriter.close();

        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }


        try {
            FileWriter writer = new FileWriter(newFileName);
            PrintWriter printer = new PrintWriter(writer);
            FileReader reader = new FileReader(originalFileName);
            BufferedReader in = new BufferedReader(reader);
            String line;

            while ((line = in.readLine()) != null) {
                printer.println(line);
            }
            printer.flush();

            printer.close();
            writer.close();

            in.close();
            reader.close();
            return true;

        } catch (FileNotFoundException e) {
            System.out.println("The file " + originalFileName + " could not be found.");
            return false;
        } catch (Exception e) {
            return false;
        }

    }




}