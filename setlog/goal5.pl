consult('/home/jmesuro/fastest/setlog/setlog495-16b.pl').
set_prolog_flag(toplevel_print_options, [quoted(true), portray(true)]).
use_module(library(dialect/sicstus/timeout)).
set_prolog_flag(answer_write_options, [quoted(true), portray(true),max_depth(0)]).
time_out(setlog( 
NAT = int(0, 2147483649) & 
NAT1 = int(1, 2147483647) & 
INT = int(-2147483648, 2147483647) & 
BIN = {cc681no,cc700yes} & 
CMODE = {cc694COF,cc698CON} & 
CTYPE = {cc708RM,cc688SC,cc685IDA,cc690SDA,cc702TD,cc703RC,cc692MD,cc695ML,cc683LP} & 
pfun(Memp) & 
dom(Memp,AUX142) & 
AUX142 = int(1,AUX143) & 
AUX143 in NAT & 
pfun(Memd) & 
dom(Memd,AUX144) & 
AUX144 = int(1,AUX145) & 
AUX145 in NAT & 
Mdp in NAT & 
Mep in NAT & 
Ped in NAT & 
Ctime in NAT & 
Acquiring in BIN & 
Waiting in BIN & 
Sending in BIN & 
Dumping in BIN & 
Waitsignal in BIN & 
Mode in CMODE & 
Ccmd in CTYPE & 
Waiting = cc681no & 
Ccmd = cc702TD & 
Sending = cc700yes & 
Dumping = cc681no & 
Ped = 43 & 
Memp neq {} & 
AUX146 in INT & 
AUX146 is Mdp + 1 & 
AUX147 in INT & 
AUX147 is Mdp + 43 & 
AUX148 = int(AUX146,AUX147) & 
inters(AUX148,AUX142,AUX149) & 
AUX149 neq {} & 
ssubset(AUX142,AUX148)
,_CONSTR),1000,_RET).